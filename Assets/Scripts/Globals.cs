﻿namespace PiratesPatron
{
	using System;

	public sealed class Globals {

		private Globals() {
			throw new NotSupportedException ();
		}

		public sealed class Tags {
			private Tags(){
				throw new NotSupportedException();
			}

			public const  String ROCK_TAG = "Rock";

			public const  String ENEMY_TAG = "Enemy";
		}

		public sealed class Scenes {
			private Scenes(){
				throw new NotSupportedException();
			}

			public static String getMainMenu(){
				#if UNITY_IOS
				return IOS_MAIN_MENU;
				#else
				return STANDALONE_MAIN_MENU;
				#endif
			}

			public static String getLevel(){
				#if UNITY_IOS
				return IOS_LEVEL;
				#else
				return STANDALONE_LEVEL;
				#endif
			
			}

			public static String getGameOver(){
				#if UNITY_IOS
				return IOS_GAME_OVER;

				#else
				return STAND_ALONE_GAME_OVER;
				#endif 

			}

			public const   string IOS_MAIN_MENU = "IpadMainMenu";

			public const  string STANDALONE_MAIN_MENU = "sc_main";

			public const  string IOS_LEVEL = "IPadScene";

			public const   string STANDALONE_LEVEL = "WindowsLevel";

			public const  string IOS_GAME_OVER = "IpadGameOver";

			public  const  string STAND_ALONE_GAME_OVER = "sc_game_over";
		
		}

		public const  float MIN_BOUNDS = 0.1f;

		public const  float MAX_BOUND = 0.9f;
	}
}