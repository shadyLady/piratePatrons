﻿namespace PiratesPatron{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class CameraController : MonoBehaviour {

		private float current_magnitude = 0.0f;

		private float current_duration = 0.0f;

		private Vector3 oldPosition;

		private void Start(){
			this.oldPosition = Camera.main.transform.position;
		}

		public void shakeScreen(float magnitude, float duration){
			if (current_duration < duration && current_magnitude < magnitude) {
				this.current_duration = duration;
				this.current_magnitude = magnitude;
				StartCoroutine ("Shake");
			}
		}

		private IEnumerator Shake() {

			float elapsed = 0.0f;

			while (elapsed < current_duration) {

				elapsed += Time.deltaTime;          

				float percentComplete = elapsed / current_duration;         
				float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

				// map value to [-1, 1]
				float x = Random.value * 2.0f - 1.0f;
				float y = Random.value * 2.0f - 1.0f;
				x *= current_magnitude * damper;
				y *= current_magnitude * damper;

				Camera.main.transform.position = new Vector3(oldPosition.x + x, oldPosition.y + y,oldPosition.z);
				yield return null;
			}
			current_duration = 0;
			current_magnitude = 0;
			Camera.main.transform.position = oldPosition;
		}
			

	}

}