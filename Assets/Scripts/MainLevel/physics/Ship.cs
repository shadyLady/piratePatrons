﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PiratesPatron{
	namespace Physics{
		public abstract class Ship : MonoBehaviour
		{

			public GameObject crashParticle;
		    protected Rigidbody m_Rigidbody;
            protected Collider m_Collider; 
		    private float m_MaxForce = 500;

            [SerializeField] private AudioClip m_AudioClip; 

		    public virtual void Awake()
		    {
		        m_Rigidbody = GetComponent<Rigidbody>();
                m_Collider = GetComponent<Collider>(); 
		    }

		    public virtual void AddForce(Vector3 force)
		    {
                if(m_Rigidbody != null)
                {
                    force.y = 0.0f;
                    m_Rigidbody.AddForce(force);
                    PiratesPatronManager.Instance.PlaySoundEffect(m_AudioClip);
                }
		    }

            protected void Sink()
            {
				if (crashParticle != null) {
					GameObject p = Instantiate (crashParticle, this.transform.position, Quaternion.identity);
					Destroy (p, 3);
				}
                m_Collider.enabled = false;
                m_Rigidbody.constraints = RigidbodyConstraints.None; 
            }
		}
	}

}