﻿namespace PiratesPatron
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public delegate void VoidDelegate();

    public class Treasure : MonoBehaviour
    {
        private Animator m_Animator;

        public ParticleSystem m_ParticleSystem;
        public int m_ParticleCount = 30; 

        private event VoidDelegate m_OnTreasurePickedUp;
        public VoidDelegate OnTreasurePickedUp
        {
            get { return m_OnTreasurePickedUp; }
            set { m_OnTreasurePickedUp = value; }
        }

        private bool m_Deactivate = false; 

        private void Awake()
        {
            m_Animator = GetComponent<Animator>(); 
        }

        public void Activate()
        {
            m_Deactivate = false; 
            gameObject.SetActive(true);
        }

        public void OnTriggerEnter(Collider col)
        {
            if(col.tag == "Player")
            {
                PiratesPatronManager.Instance.addScore();

                m_ParticleSystem.Emit(m_ParticleCount); 
                m_Animator.SetTrigger("Open");
                m_Deactivate = true; 
                Invoke("Deactivate", 3.0f);
            }

        }

        private void Deactivate()
        {
            if (m_OnTreasurePickedUp != null)
                m_OnTreasurePickedUp();

            if(m_Deactivate)
                gameObject.SetActive(false);
        }
    }
}



