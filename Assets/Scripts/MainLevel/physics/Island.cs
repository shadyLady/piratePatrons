﻿namespace PiratesPatron
{
    using UnityEngine;

    public class Island : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_Harbour;
        [SerializeField]
        private float m_DistanceFromHarbour;

		[SerializeField]
		private GameObject explosionPrefab;


        public AudioClip m_IslandHit; 

        public void OnHit()
        {
            Vector3 harbourPosition = m_Harbour.transform.TransformPoint(-m_Harbour.transform.forward * m_DistanceFromHarbour);
            PiratesPatronManager.Instance.m_EnemyManager.SpawnEnemy(harbourPosition);
            //CameraController.Instance.shakeScreen(duration: 1, magnitude: 0.3f);

			if (explosionPrefab != null) {
				PiratesPatronManager.Instance.ShakeScreen (2, 1);
				GameObject dest = Instantiate (explosionPrefab, this.transform.position, Quaternion.identity);
				Destroy (dest, 3);
			}

            PiratesPatronManager.Instance.PlaySoundEffect(m_IslandHit);
        }
    }
}


