﻿
namespace PiratesPatron
{
    namespace Physics
    {
        using System.Collections;
        using System.Collections.Generic;
        using UnityEngine;

        public class Player : Ship
        {
            private Vector3 m_StartPosition;

            public AudioClip m_CrashOnRocks;
            private AudioClip m_CrashOnEnemy; 

            private void Start()
            {
                m_StartPosition = transform.position; 
            }

            private void OnCollisionEnter(Collision col)
            {
                if(col.gameObject.tag == Globals.Tags.ROCK_TAG )
                {
                    PiratesPatronManager.Instance.PlaySoundEffect(m_CrashOnRocks);
					PiratesPatronManager.Instance.ShakeScreen(2,1);
			
                    Sink();

                    Invoke("Reset", 1.5f);
                }

                if(col.gameObject.tag == Globals.Tags.ENEMY_TAG)
                {
                    PiratesPatronManager.Instance.PlaySoundEffect(m_CrashOnEnemy);
					PiratesPatronManager.Instance.ShakeScreen(2,1);
                    Sink();

                    Invoke("Reset", 1.5f);
                }
            }

            private void Reset()
            {
	
                m_Collider.enabled = true;

                m_Rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                m_Rigidbody.velocity = Vector3.zero;
                m_Rigidbody.angularVelocity = Vector3.zero; 
				PiratesPatronManager.Instance.GoToGameOver ();
    //            transform.position = m_StartPosition; 
		
            }
        }
    }
}
