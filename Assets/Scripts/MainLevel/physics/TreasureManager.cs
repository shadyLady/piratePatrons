﻿namespace PiratesPatron
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TreasureManager : MonoBehaviour
    {
        private List<Treasure> m_TreasureList;
        private Treasure m_ActiveTreasure;

        [SerializeField]
        private GameObject m_TreasureHighlight;

        [SerializeField] private AudioClip[] m_TreasureClips;

        private int m_Start = 0; 

        private void Start()
        {
            ListTreasures();
            SpawnTreasure();
        }

        private void ListTreasures()
        {
            m_TreasureList = new List<Treasure>();

            for (int i = 0; i < transform.childCount; i++)
            {
                m_TreasureList.Add(transform.GetChild(i).GetComponent<Treasure>());
            }
        }
        private void SpawnTreasure()
        {
            int id = Random.Range(0, m_TreasureList.Count);

			if (m_Start == 0) {
				m_Start = 1; 
			} else {
				PlayClips ();
			}

            if (m_ActiveTreasure != null)
            {
                m_ActiveTreasure.OnTreasurePickedUp -= SpawnTreasure;
            }

            m_TreasureList[id].Activate();
            m_TreasureList[id].OnTreasurePickedUp += SpawnTreasure;

            m_ActiveTreasure = m_TreasureList[id];

            m_TreasureHighlight.transform.position = m_ActiveTreasure.transform.position;
        }
        private void PlayClips()
        {
            for(int i = 0; i < m_TreasureClips.Length; i++)
            {
                PiratesPatronManager.Instance.PlaySoundEffect(m_TreasureClips[i]);
            }
        }
    }
}

