﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; 

namespace PiratesPatron{
	namespace Physics{

		public class Enemy : Ship
		{
		    private NavMeshAgent m_Agent; 

            private Vector3 m_DesiredPosition;
            private float m_MoveSpeed = 200;

            public AudioClip m_CrashOnRocks;
            public AudioClip m_CrashOnEnemy;

            private EnemyManager m_EnemyManager; 

            public void OnCreate(EnemyManager e)
            {
                m_EnemyManager = e; 
            }

		    public override void Awake()
		    {
                base.Awake();
		        m_Agent = GetComponent<NavMeshAgent>();
                m_Agent.speed = 3.0f; 
		    }

            private void OnEnable()
            {
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
                transform.rotation = Quaternion.Euler(Vector3.zero);
               // m_Agent.enabled = true;
                m_Collider.enabled = true; 
            }
            public void Move(Vector3 dir)
            {
                m_Agent.SetDestination(dir);
                /*
                m_DesiredPosition = dir;//0 * m_MoveSpeed;// * Time.deltaTime;

                if(m_Agent.isActiveAndEnabled)
                    m_Agent.destination = m_DesiredPosition;*/
            }

            private void FixedUpdate()
            {
                if (m_Rigidbody.velocity != Vector3.zero)
                {
                    m_Rigidbody.velocity *= 0.99f;
                }
            }

            private void OnCollisionEnter(Collision col)
            {
                if (col.gameObject.tag == Globals.Tags.ROCK_TAG)
                {
                    PiratesPatronManager.Instance.PlaySoundEffect(m_CrashOnRocks);
                    Sink();

                   // m_Agent.enabled = false; 
                    Invoke("Remove", 1.5f);
                }
                 
                if(col.gameObject.tag == "Boundary")
                {
                    m_Rigidbody.velocity = Vector3.zero; 
                }
            }

            private void Remove()
            {
                m_EnemyManager.RemoveEnemy(this);
                gameObject.SetActive(false);
            }
        }
	}
}