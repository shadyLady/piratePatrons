﻿
namespace PiratesPatron{
	namespace Physics{
		using System.Collections;
		using System.Collections.Generic;
		using UnityEngine;

		public class Wave : MonoBehaviour
		{
		    private CapsuleCollider m_CapsuleCollider;
		    [SerializeField] private GameObject m_Ring;

		    [SerializeField] private GameObject[] m_RingList;

		    [SerializeField] private float m_MinSize;
		    [SerializeField] private float m_MaxSize;
		    private float m_CurrentSize;

		    [SerializeField] private float m_WaveSpeed;
		    [SerializeField] private float m_MaxForce;

			[SerializeField]
			private float clickShakeMagnitude = 0.2f;

			[SerializeField]
			private float clickShakeDuration = 0.3f;

		    private bool m_IsActive = false;

            [SerializeField] private AudioClip m_ClickSound; 

		    private void Awake()
		    {
		        m_CapsuleCollider = GetComponent<CapsuleCollider>(); 
		    }

		    private void Update()
		    {
		        if(Input.GetMouseButtonDown(0))
		        {
					PiratesPatronManager.Instance.ShakeScreen (clickShakeMagnitude, clickShakeDuration);
		            Vector3 hitPosition = GetHitPosition();

		            transform.position = hitPosition;

		            m_CapsuleCollider.enabled = true; 
		            m_Ring.SetActive(true); 
		            m_IsActive = true;

                    PiratesPatronManager.Instance.PlaySoundEffect(m_ClickSound);
		        }

		        if (m_IsActive)
		            Grow(); 

		        if(Input.GetMouseButtonUp(0))
		        {
		            m_CapsuleCollider.enabled = false;
		            m_CurrentSize = m_MinSize;
		            m_Ring.SetActive(false);
		            m_IsActive = false;
		        }
		    }

		    private Vector3 GetHitPosition()
		    {
		        Vector3 hitPosition = Vector3.zero;
		        RaycastHit hit;

		        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		        ray.origin = Camera.main.transform.position;

		        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		        {
		            hitPosition = hit.point;
		        }

		        return hitPosition;
		    }
		    private void Grow()
		    {
		        if (m_CurrentSize < m_MaxSize)
		            m_CurrentSize += Time.deltaTime * m_WaveSpeed;

		        m_CapsuleCollider.radius = m_CurrentSize * 0.5f;

		        m_Ring.transform.localScale = new Vector3(
		            m_CurrentSize,
		            m_CurrentSize,
		            m_CurrentSize);
		    }
		    private void OnTriggerEnter(Collider col)
		    {
		        Ship ship = col.GetComponent<Ship>();

		        if(ship != null)
		        {
		            Vector3 dir = col.transform.position - transform.position;
		            dir.Normalize(); 

		            ship.AddForce(dir * m_MaxForce);
                    return; 
		        }

                Island island = col.GetComponent<Island>(); 

                if(island != null)
                {
                    island.OnHit(); 
                }
		    }
		}
	}

}