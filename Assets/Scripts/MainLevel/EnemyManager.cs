﻿namespace PiratesPatron{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Physics;
	using System;

	public class EnemyManager : MonoBehaviour
	{
	    [SerializeField] private GameObject m_PlayerBoat;

	    [SerializeField] private Enemy m_EnemyPrefab; 
	    private Enemy[] m_EnemyList; 
	    private int m_MaxEnemies = 50;

	    private List<Enemy> m_ActiveEnemies = new List<Enemy>(); 

	    private void Start()
	    {
	        ReadyEnemies();
	    }

	    private void Update()
	    {
	            MoveEnemies(); 
	    }
	    private void MoveEnemies()
	    {
	        for(int i = 0; i < m_ActiveEnemies.Count; i++)
	        {
	            //Vector3 direction = m_PlayerBoat.transform.position - m_ActiveEnemies[i].transform.position;
	            //direction.Normalize();

	            m_ActiveEnemies[i].Move(m_PlayerBoat.transform.position);
	        }
	    }
	    private void ReadyEnemies()
	    {
	        m_EnemyList = new Enemy[m_MaxEnemies];

	        for (int i = 0; i < m_MaxEnemies; i++)
	        {
	            m_EnemyList[i] = Instantiate(m_EnemyPrefab);
	            m_EnemyList[i].transform.parent = transform;
	            m_EnemyList[i].name = "Enemy_" + i;
                m_EnemyList[i].OnCreate(this);
	        }
	    }
	    public void SpawnEnemy(Vector3 spawnPosition)
	    {
	        for(int i = 0; i < m_EnemyList.Length; i++)
	        {
	            if(!m_EnemyList[i].isActiveAndEnabled)
	            {
	                m_EnemyList[i].transform.position = spawnPosition;
                    m_EnemyList[i].gameObject.SetActive(true);
	                m_ActiveEnemies.Add(m_EnemyList[i]);
	                return;
	            }
	        }
	    }
	    public void RemoveEnemy(Enemy enemy)
	    {
	        if(m_ActiveEnemies.Contains(enemy))
	        {
	            m_ActiveEnemies.Remove(enemy);
	        }
	    }

	    private void Reset()
	    {
	        if(m_ActiveEnemies.Count != 0)
	        {
	            for(int i = 0; i < m_ActiveEnemies.Count; i++)
	            {
					throw new NotImplementedException ("Not yet Implemented");
	                //m_ActiveEnemies[i].Deactivate(); 
	            }
	        }
	    }
	}
}