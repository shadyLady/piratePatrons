﻿namespace PiratesPatron{
	using UnityEngine;
    using UnityEngine.UI; 
	using UnityEngine.SceneManagement;

	public sealed class PiratesPatronManager : MonoBehaviour {

		private static PiratesPatronManager instance;

        public Text m_ScoreText; 
		public EnemyManager m_EnemyManager { 
			get;
			private set;
		}

		private CameraController cameraController;

		private AudioSource m_AmbientSource;

		private AudioSource[] m_SoundEffectsSources;

		private int m_EffectSourceAmount = 10;

		public int score { private set; get; }


		public void GoToGameOver(){
			SceneManager.LoadScene (Globals.Scenes.getGameOver());
		}

		private void Update(){
			if (Input.GetKey (KeyCode.Escape)) {
				SceneManager.LoadScene (Globals.Scenes.getMainMenu());
			}
		}

		public void ShakeScreen(float magnitude, float duration){
			this.cameraController.shakeScreen (magnitude, duration);
		}

		public void addScore() {
			score++;
            m_ScoreText.text = score.ToString(); 
		}

		public void resetScore(){
			score = 0;
            m_ScoreText.text = score.ToString(); 
		}


		private void Awake(){

			this.m_EnemyManager = this.GetComponent<EnemyManager> ();
			this.cameraController = this.GetComponent<CameraController> ();

			if(null != instance){
				Destroy (instance);
			}

			instance = this;
			SetupAudio ();
		}



		public static PiratesPatronManager Instance
		{
			get { return instance; }
		}



		public void PlaySoundEffect(AudioClip clip)
		{
			for(int i = 0; i < m_SoundEffectsSources.Length; i++)
			{
				if(!m_SoundEffectsSources[i].isPlaying)
				{
					m_SoundEffectsSources[i].PlayOneShot(clip);
					return; 
				}
			}
		}

		private void SetupAudio(){
			m_AmbientSource = gameObject.AddComponent<AudioSource>();

			m_SoundEffectsSources = new AudioSource[m_EffectSourceAmount];

			for(int i = 0; i < m_EffectSourceAmount; i++)
			{
				m_SoundEffectsSources[i] = gameObject.AddComponent<AudioSource>(); 
			}
		}



	}
}