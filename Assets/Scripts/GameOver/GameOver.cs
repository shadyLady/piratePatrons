﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

namespace PiratesPatron{
	public class GameOver : MonoBehaviour {

		[SerializeField]
		private Text treasureScores;

		private void displayScore(int score = -1){
			treasureScores.text = String.Format ("You found : {0}", score);
		}

		private void Start(){
			displayScore(PiratesPatronManager.Instance.score);

		}



		private void Update(){

			/*#if UNITY_IOS

			if (Input.GetMouseButton (0)) {
				SceneManager.LoadScene ("sc_main");
			}
			#endif*/
			if (Input.GetKey (KeyCode.Escape)) {
				if (Input.GetKey (KeyCode.Escape)) {
                    ReturnToMain(); 
				}

				SceneManager.LoadScene (Globals.Scenes.getMainMenu());
			}
		}

        public void Restart()
        {
			SceneManager.LoadScene (Globals.Scenes.getLevel());
        }

        public void ReturnToMain()
        {
			SceneManager.LoadScene (Globals.Scenes.getMainMenu());
        }
	}
}