﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace PiratesPatron{

	public class MainMenu : MonoBehaviour {

		public GameObject audioManager;

        public AudioClip audioClip;
        public AudioClip mutanyClip;

        public CanvasGroup m_BlackScreen; 

		private void Awake(){
			if (!GameObject.Find ("AudioManager")) {
				GameObject res = Instantiate (audioManager);
				res.name = "AudioManager";
				DontDestroyOnLoad (res);
			}
		}

		public void StartGame(){
            PiratesPatronManager.Instance.PlaySoundEffect(audioClip);
            StartCoroutine(FadeToBlack(m_BlackScreen));
            Invoke("LoadScene", audioClip.length);
		}

		public void QuitGame(){
            PiratesPatronManager.Instance.PlaySoundEffect(mutanyClip);
            Invoke("Quit", mutanyClip.length);
		}
        private void Quit()
        {
            Application.Quit(); 
        }
        private void LoadScene()
        {

			SceneManager.LoadScene (Globals.Scenes.getLevel ());
        }

        private IEnumerator FadeToBlack(CanvasGroup cg)
        {
            float time = 0.0f; 

            while(time < 1.0f)
            {
                time += Time.deltaTime;
                cg.alpha = time;

                yield return new WaitForSeconds(Time.deltaTime);
            }

            yield return null; 
        }
	}
	

}