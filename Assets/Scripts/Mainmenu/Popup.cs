﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PiratesPatron; 

public class Popup : MonoBehaviour
{
    public AudioClip m_PopUpOpen; 

    public void Activate()
    {
        PiratesPatronManager.Instance.PlaySoundEffect(m_PopUpOpen);
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
